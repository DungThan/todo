<?php
define('PATH_ROOT', __DIR__);

spl_autoload_register(function (string $className) {
    include_once PATH_ROOT . '/' . $className . '.php';
});

$router = new Route\Route();
include_once PATH_ROOT . '/Config/routes.php';

$requestUrl = !empty($_GET['url']) ? '/' . $_GET['url'] : '/';

$methodUrl = !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

// map URL
$router->map($requestUrl, $methodUrl);