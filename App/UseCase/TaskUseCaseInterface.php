<?php
namespace App\UseCase;

use App\Entity\Task;
use App\Repository\TaskRepositoryInterface;

interface TaskUseCaseInterface {
    public function __construct(TaskRepositoryInterface $repository){}

    public function findAll() {}

    public function find($id) {}

    public function store(Task $task) {}

    public function changeStatus(Task $task) {}

    public function deleteAllTaskDone() {}
}