<?php
namespace App\UseCase;

use App\Entity\Task;
use App\Repository\TaskRepositoryInterface;

class TaskUseCase implements TaskUseCaseInterface {

    private $repository;

    public function __construct(TaskRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function findAll() 
    {
        return $this->repository->findAll();
    }

    public function store(Task $task)
    {
        return $this->repository->store($task);
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }

    public function changeStatus(Task $task) {
        if (!$task) return false;
        if ($task->status == Task::DONE) $task->status = Task::UNDONE;
        else $task->status = Task::DONE;
        return $this->store($task);
    }

    public function deleteAllTaskDone()
    {
        $task = $this->repository->find($id);
        if ($task) return $this->repository->delete($id);
        return false;
    }
}