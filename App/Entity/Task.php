<?php
namespace App\Entity;

class Task  implements Entity {
    public $id;
    public $title;
    public $description;
    public $performer;
    public $status;
}