<?php
namespace App\Controller;

class TaskController {
    private $useCase;

    public function __construct(TaskUseCaseInterface $useCase) {
        $this->useCase = $useCase;
    }

    public function index() {
        $tasks = $this->useCase->getAll();
        return view('file list task', TaskViewModel($tasks));
    }

    public function changeStatus($id) {
        $task = $this->useCase->find($id);
        $task = $this->useCase->changeStatus($task);
        return json(['task' => TaskViewModel($task)]);
    }

    public function deleteAllTaskDone() {
        $result = $this->useCase->deleteAllTaskDone();
        if ($result) return json(['status' => 'success']);
        return json(['status' => 'error']);
    }
}