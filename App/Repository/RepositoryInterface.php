<?php
namespace App\Repository;

use App\Entity\Entity;

interface RepositoryInterface {
    public function getAll();

    public function find(int $id);

    public function store(Entity $entity);

    public function delete(int $id);
}